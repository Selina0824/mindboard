import React, {Component}  from 'react';
import { Redirect, withRouter }  from 'react-router-dom'
import { Icon } from 'antd';

import { getStorage, removeStorage} from '../../utils/utils'
import './styles.css';
import logo from '../../assets/images/logo.png';
import avatar from '../../assets/images/avatar.jpg';  // TODO 从接口中获取

@withRouter
class TopBar extends Component {
    componentDidMount(){
        let access_token = getStorage('access_token');
        // let expireTime = getStorage('expireTime_maintenance');
        // let isValidToken = access_token && moment(new Date()).isBefore(expireTime);
        // if(isValidToken) this.props.loginSuccess()
        // this.props.dispatchCurrentProject({});
    }
    onLogout = ()=>{
        removeStorage('access_token');
        this.props.history.push('/login');
        
    }
    render(){
        const _avatar = this.props.authUser && this.props.authUser.userOrgMap && this.props.authUser.userOrgMap.length && this.props.authUser.userOrgMap[0].avatar;
        return (
            <div className='header-container'>
            {/* {this.props.isAuth?  null: <Redirect to = '/login'></Redirect>} */}
                <div className='header-left'>
                    <img className='logo' src={logo} alt='logo'/>
                    <div className='title'>MindBoard</div>
                </div>
                <div className='header-right' >
                    <div className='user-info'>
                        <img className='avatar' src={_avatar || avatar} alt='avatar'/>
                        <div className='user'>
                            <div>Qian Liu</div>
                            <div>DI SW CAS MP EMK DO-CHN DEV</div>
                        </div>
                    </div>
                    <Icon type='logout' style={{color:'grey', fontSize:'28px',marginTop:'10px' }} onClick={this.onLogout}/>
                </div>
                
            </div>
        )
    }
}

export default TopBar

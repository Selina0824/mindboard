import React, {Component}  from 'react';
import { Table} from 'antd';
import './styles.css';

class MainTable extends Component{
    render(){
       return (
        <Table 
            rowClassName = 'main-table-row'
            pagination={false}
            className= 'main-table'
            title={this.props.title}
            columns={this.props.columns} 
            dataSource={this.props.dataSource} />
       )
    }
}

export default MainTable
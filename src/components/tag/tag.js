import React, {Component}  from 'react';
import './styles.css';

class Tag extends Component {
    render(){
        return (
                <div className="tag tag-lime">
                    <a className="tag-addon tag-lime"  href="https://code.siemens.com/mindsphere-mainline/coreservices/iam/IdentityManagement/issues/9" target="_blank" rel="noopener noreferrer">
                        <span data-toggle="tooltip" title="Deployed at 2019-08-09 01:49 UTC">Deployed</span>
                    </a>
                    <span className="tag-addon tag-gray">3.21.7</span>
                </div>
        )
    }
}

export default Tag
import React, {Component}  from 'react';
import TopBar from '../topbar/topbar';
import './layout.css';

class HomeLayout extends Component {
    // constructor(props){
    //     super(props)
    // }

    render(){
        return (
            <div className=''>
                <TopBar/>
                {this.props.children}
            </div>
        )
    }
}

export default HomeLayout
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Auth from './pages/auth/index'
import ErrorPage from './pages/404/index'
import * as serviceWorker from './serviceWorker';
import './config';
import { HashRouter, Route, Switch, Redirect} from 'react-router-dom'

ReactDOM.render(
            <HashRouter>
                    <Switch>
                        <Redirect exact from='/' to = '/home'/>
                        <Route path='/home' component={App}/>
                        <Route path='/login' component={Auth}/>
                        <Route  component = {ErrorPage}/>
                    </Switch>
            </HashRouter>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

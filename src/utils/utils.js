  // 设置本地存储
  export function setStorage(name, data) {
    let dataType = typeof data;
    // JSON对象
    if (dataType === 'object') {
        window.localStorage.setItem(name, JSON.stringify(data));
    } else if (['number', 'string', 'boolean'].indexOf(dataType) >= 0) {
        window.localStorage.setItem(name, data);
    } else {
        console.log('该类型不能用于本地存储');
      //   console.log(data)
    }
}
// 取出本地存储内容
export function getStorage(name) {
    let data = window.localStorage.getItem(name);
    if (data) {
       try{
          return JSON.parse(data);
       }catch(err){
           return data
       }
    } else {
        return '';
    }
}
// 删除本地存储内容
export function removeStorage(name) {
    window.localStorage.removeItem(name);
}

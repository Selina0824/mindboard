import React, {Component} from 'react';
import { Row, Col ,Button, Avatar, Tooltip} from 'antd';
import axios from 'axios';
import HomeLayout from './components/layout/home.layout';
import Table from './components/table/index';
import Tag from './components/tag/tag';
import './App.css';

class App extends Component {

    constructor(){
        super();
        this.state={

        }
    }
    componentDidMount(){
        axios.get('/api/v4/projects/79546/issues')
        axios.get('/api/v4/projects/79546/repository/files/services.json?ref=master').then(res=>{
            const servicesInfo = JSON.parse(window.atob(res.data.content));
            this.setState(servicesInfo);
            // this.generateTableDatas(servicesInfo);
        })
    }

    // generateTableDatas(servicesInfo){
    //     const {areas} =  servicesInfo;
    //     let areaTitle, dataSources;
    //     areas.map(area=>{
    //         areaTitle = area.title;
    //         dataSources = area.services.map(service=>{
    //             return {

    //             }
    //         })
    //     })
    // }



    render(){

        const columns = [
            {
              title: 'PO',
              dataIndex: 'team',
              key: 'team',
              render:(text,record) => (
                <Tooltip title='deployer name'>
                    <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                </Tooltip>
              )
            },
            {
              title: 'SERVICE',
              dataIndex: 'name',
              key: 'name',
            },
            {
              title: 'INTEG',
              key: 'address',
              width:'20%',
              render: (text, record) => (
                <Tag/>
              ),
            },
            {
              title: 'CERT',
              key: 'ad',
              width:'20%',
              render: (text, record) => (
                <Button type="primary" className='loading-btn' size="small" loading>
                    Loading
                </Button>
              ),
            },
            {
              title: 'PROD',
              key: 'a',
              width:'20%',
              render: (text, record) => (
                  <Tag/>
              ),
            },
          ];

        return (
            <HomeLayout>
                <div className="App">
                    <div className='dashboard'>
                        <Row gutter={16}>
                            {
                            this.state.environments && this.state.environments.map(env=>{
                                    return  <Col className="gutter-row"  xs={20} sm={16} md={12} lg={8} xl={6} key={env.environmentName}>
                                                <div className='card'>
                                                    <div className='card-body text-center'>
                                                        <div className="h5">{env.environmentName}</div>
                                                        <div className="num">160</div>
                                                    </div>
                                                </div>
                                            </Col>
                                })
                            }
                        </Row>
                    </div>
                    <div>
                        {
                            this.state.areas && this.state.areas.map(area=>{
                                return (
                                    <div key={area.title} className='area-table-container'>
                                        <Table title={()=>area.title} columns={columns} dataSource={area.services.map(ele=>({...ele,key: ele.name}))}/>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </HomeLayout>
          );
    }
}

export default App;
